﻿namespace ManiaXmlRpc
{
	public class Constants
	{
		public class ServerMethods
		{
			public const string Authenticate = "Authenticate";

			/// <summary>
			/// For use with TrackMania Forever servers only
			/// </summary>
			public class Challenge
			{
				public const string GetCurrentChallengeIndex = "GetCurrentChallengeIndex";
				public const string SetNextChallengeIndex = "SetNextChallengeIndex";
				public const string NextChallenge = "NextChallenge";
				public const string RestartChallenge = "RestartChallenge";

				public const string GetCupRoundsPerChallenge = "GetCupRoundsPerChallenge";
				public const string SetCupRoundsPerChallenge = "SetCupRoundsPerChallenge";
			}

			public class Chat
			{
				public const string SendServerMessage = "ChatSendServerMessage";
				public const string SendServerMessageToLogin = "ChatSendServerMessageToLogin";
				public const string SendToLogin = "ChatSendToLogin";
			}

			public class Cup
			{
				public const string SetCupPointsLimit = "SetCupPointsLimit";
				public const string SetCupRoundsPerMap = "SetCupRoundsPerMap";
				public const string SetCupWarmUpDuration = "SetCupWarmUpDuration";
				public const string SetCupNumberOfWinners = "SetCupNbWinners";

				public const string GetCupPointsLimit = "GetCupPointsLimit";
				public const string GetCupRoundsPerMap = "GetCupRoundsPerMap";
				public const string GetCupWarmUpDuration = "GetCupWarmUpDuration";
				public const string GetCupNumberOfWinners = "GetCupNbWinners";
			}

			public class Game
			{
				public const string SetGameMode = "SetGameMode";
				public const string GetGameMode = "GetGameMode";
			}

			public class Manialink
			{
				public const string SendDisplayManialinkPage = "SendDisplayManialinkPage";
				public const string SendHideManialinkPage = "SendHideManialinkPage";
			}

			public class ManiaPlanet
			{
				public const string BeginMap = "ManiaPlanet.BeginMap";
				public const string EndMap = "ManiaPlanet.EndMap";
				public const string BeginMatch = "ManiaPlanet.BeginMatch";
				public const string EndMatch = "ManiaPlanet.EndMatch";
				public const string PlayerChat = "ManiaPlanet.PlayerChat";

				public class Script
				{
					public const string TriggerModeScriptEventArray = "TriggerModeScriptEventArray";
					public const string ManiaPlanet_ModeScriptCallbackArray = "ManiaPlanet.ModeScriptCallbackArray";
					public const string XmlRpc_EnableCallbacks = "XmlRpc.EnableCallbacks";

					public class ScriptCallback
					{
						public const string TrackMania_Scores = "Trackmania.Scores";
						public const string ManiaPlanet_EndTurn_Start = "Maniaplanet.EndTurn_Start";
						public const string ManiaPlanet_EndRound_End = "Maniaplanet.ManiaPlent_EndRound_End";
						public const string ManiaPlanet_EndRound_Start = "Maniaplanet.ManiaPlent_EndRound_Start";
						public const string ManiaPlanet_EndTurn_End = "Maniaplanet.ManiaPlent_EndTurn_End";
						public const string ManiaPlanet_EndPlayLoop = "Maniaplanet.ManiaPlent_EndPlayLoop";
						public const string ManiaPlanet_EndMap_Start = "Maniaplanet.ManiaPlent_EndMap_Start"; // This one includes player rankings
						public const string ManiaPlanet_StartMap_End = "Maniaplanet.StartMap_End";
						public const string ShootMania_Scores = "Shootmania.Scores";
					}
				}
			}

			public class Map
			{
				public class TrackManiaForever
				{
					public const string GetCurrentChallengeIndex = "GetCurrentChallengeIndex";
					public const string GetCurrentChallengeInfo = "GetCurrentChallengeInfo";
				}

				public const string GetCurrentMapInfo = "GetCurrentMapInfo";
				public const string GetCurrentMapIndex = "GetCurrentMapIndex";
				public const string SetNextMapIndex = "SetNextMapIndex";
				public const string NextMap = "NextMap";
				public const string RestartMap = "RestartMap";
				public const string GetCurrentRanking = "GetCurrentRanking";
			}

			public class Notice
			{
				public const string SendNoticeToLogin = "SendNoticeToLogin";
			}

			public class Players
			{
				public const string GetMaxPlayers = "GetMaxPlayers";
				public const string GetPlayerList = "GetPlayerList";
				public const string GetPlayerInfo = "GetPlayerInfo";
			}

			public class Rounds
			{
				public const string SetRoundForcedLaps = "SetRoundForcedLaps";
				public const string SetRoundPointsLimit = "SetRoundPointsLimit";
				public const string SetRoundUseNewRules = "SetUseNewRulesRound";
				/// <summary>
				/// Not supported yet...
				/// </summary>
				public const string SetRoundCustomPoints = "SetRoundCustomPoints";

				public const string GetRoundForcedLaps = "GetRoundForcedLaps";
				public const string GetRoundPointsLimit = "GetRoundPointsLimit";
				public const string GetRoundUseNewRules = "GetUseNewRulesRound";
				/// <summary>
				/// Not supported yet...
				/// </summary>
				public const string GetRoundCustomPoints = "GetRoundCustomPoints";
			}

			public class Server
			{
				public const string GetServerName = "GetServerName";
				public const string SetServerName = "SetServerName";
				public const string GetVersion = "GetVersion";
				public const string SetApiVersion = "SetApiVersion";
			}

			public class System
			{
				public const string ListMethods = "system.listMethods";
				public const string MethodSignature = "system.methodSignature";
				public const string MethodHelp = "system.methodHelp";
				public const string MultiCall = "system.multicall";
			}

			public class TimeAttack
			{
				public const string SetTimeAttackSynchStartPeriod = "SetTimeAttackSynchStartPeriod";
				public const string SetTimeAttackLimit = "SetTimeAttackLimit";
			}

			public class TrackMania
			{
				public const string BeginRace = "TrackMania.BeginRace";
				public const string EndRace = "TrackMania.EndRace";
				public const string PlayerChat = "TrackMania.PlayerChat";

				public const string PlayerFinish = "TrackMania.PlayerFinish";
				public const string PlayerCheckpoint = "TrackMania.PlayerCheckpoint";
			}
		}

		public class GbxCallbackDataColumnNames
		{
			public class StartMatchMapInfo
			{
				public const string SilverTime = "SilverTime";
				public const string FileName = "FileName";
				public const string Name = "Name";
				public const string MapType = "MapType";
				public const string Author = "Author";
				public const string LapRace = "LapRace";
				public const string Mood = "Mood";
				public const string MapStyle = "MapStyle";
				public const string CopperPrice = "CopperPrice";
				public const string NumberOfLaps = "NbLaps";
				public const string NumberOfCheckpoints = "NbCheckpoints";
				public const string UId = "UId";
				public const string AuthorTime = "AuthorTime";
				public const string Environment = "Environnement";
				public const string GoldTime = "GoldTime";
				public const string BronzeTime = "BronzeTime";
			}
		}
	}
}