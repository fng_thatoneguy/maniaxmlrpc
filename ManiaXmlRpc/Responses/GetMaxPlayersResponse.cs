﻿namespace ManiaXmlRpc.Responses
{
	public class GetMaxPlayersResponse
	{
		public int CurrentValue { get; set; }
		public int NextValue { get; set; }
	}
}