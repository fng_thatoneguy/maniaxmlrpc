﻿using System.Collections.Generic;

namespace ManiaXmlRpc.Responses
{
	public class SystemMethodSignatureResponse
	{
		public string ReturnType { get; set; }
		public List<string> ParameterTypes { get; set; }
	}
}