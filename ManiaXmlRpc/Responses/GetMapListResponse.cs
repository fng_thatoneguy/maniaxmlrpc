﻿using System.Collections.Generic;

namespace ManiaXmlRpc.Models
{
	public class MapInfo
	{
		public string Name { get; set; }
		public string UId { get; set; }
		public string FileName { get; set; }
		public string Environnement { get; set; }
		public string Author { get; set; }
		public int GoldTime { get; set; }
		public int CopperPrice { get; set; }
		public string MapType { get; set; }
		public string MapStyle { get; set; }
	}

	public class GetMapListResponse : List<MapInfo>
	{}
}