﻿using Newtonsoft.Json;

namespace ManiaXmlRpc.Responses
{
	public class GetMapInfoResponse
	{
		public string Name { get; set; }
		public string UId { get; set; }
		public string FileName { get; set; }
		public string Author { get; set; }
		public string Environnement { get; set; }
		public string Mood { get; set; }

		/// <summary>
		/// Bronze Time (in milliseconds)
		/// </summary>
		public long BronzeTime { get; set; }

		/// <summary>
		/// Silver Time (in milliseconds)
		/// </summary>
		public long SilverTime { get; set; }

		/// <summary>
		/// Gold Time (in milliseconds)
		/// </summary>
		public long GoldTime { get; set; }

		/// <summary>
		/// Author Time (in milliseconds)
		/// </summary>
		public long AuthorTime { get; set; }

		public int CopperPrice { get; set; }
		public bool LapRace { get; set; }
		public int NbLaps { get; set; }
		public int NbCheckpoints { get; set; }
		public string MapType { get; set; }
		public string MapStyle { get; set; }
	}
}