using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Xml;

namespace ManiaXmlRpc
{
	public enum MessageTypes
	{
		None,
		Response,
		Request,
		Callback
	}

	public class GbxCall
	{
		private int m_handle;
		private readonly string m_xml;
		private readonly ArrayList m_params = new();
		private readonly bool m_error = false;
		private readonly string m_error_string;
		private readonly int m_error_code;
		private readonly string m_method_name;
		private readonly MessageTypes m_type;

		public static class Constants
		{
			public const string MethodCall = "methodCall";
			public const string MethodName = "methodName";
			public const string MethodResponse = "methodResponse";
			public const string Params = "params";
		}

		/// <summary>
		/// Parses a incoming message.
		/// Xml to object.
		/// </summary>
		/// <param name="in_handle"></param>
		/// <param name="in_data"></param>
		public GbxCall(int in_handle, byte[] in_data)
		{
			this.m_type = MessageTypes.None;
			this.m_handle = in_handle;
			this.m_xml = Encoding.UTF8.GetString(in_data);
			this.m_error_code = 0;
			this.m_error_string = "";

			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(this.m_xml);
			XmlElement methodParams = null;

			// message is of type request ...
			if (xmlDoc[Constants.MethodCall] != null)
			{
				// check message type ...
				if (in_handle > 0)
					this.m_type = MessageTypes.Callback;
				else
					this.m_type = MessageTypes.Request;

				// try to get the method name ...
				if (xmlDoc[Constants.MethodCall][Constants.MethodName] != null)
				{
					this.m_method_name = xmlDoc[Constants.MethodCall][Constants.MethodName].InnerText;
				}
				else
					this.m_error = true;

				// try to get the mehtod's parameters ...
				if (xmlDoc[Constants.MethodCall][Constants.Params] != null)
				{
					this.m_error = false;
					methodParams = xmlDoc[Constants.MethodCall][Constants.Params];
				}
				else
					this.m_error = true;
			}
			else if (xmlDoc[Constants.MethodResponse] != null) // message is of type response ...
			{
				// check message type ...
				this.m_type = MessageTypes.Response;

				if (xmlDoc[Constants.MethodResponse]["fault"] != null)
				{
					Hashtable err_struct = (Hashtable)ParseXml(xmlDoc[Constants.MethodResponse]["fault"]);
					this.m_error_code = err_struct["faultCode"] == null ? 0 : Convert.ToInt32(err_struct["faultCode"]);
					this.m_error_string = (string)err_struct["faultString"];
					this.m_error = true;
				}
				else if (xmlDoc[Constants.MethodResponse]["params"] != null)
				{
					this.m_error = false;
					methodParams = xmlDoc[Constants.MethodResponse]["params"];
				}
				else
				{
					this.m_error = true;
				}
			}
			else
			{
				this.m_error = true;
			}

			// parse each parameter of the message, if there are any ...
			if (methodParams != null)
			{
				foreach (XmlElement param in methodParams)
				{
					this.m_params.Add(ParseXml(param));
				}
			}
		}

		/// <summary>
		/// Parses a response message.
		/// Object to xml.
		/// </summary>
		/// <param name="in_params"></param>
		public GbxCall(object[] in_params)
		{
			this.m_xml = "<?xml version=\"1.0\" ?>\n";
			this.m_xml += "<methodResponse>\n";
			this.m_xml += "<params>\n";

			if (in_params != null)
			{
				foreach (object param in in_params)
				{
					this.m_xml += $"<param>{ParseObject(param)}</param>\n";
				}
			}

			this.m_xml += "</params>";
			this.m_xml += "</methodResponse>";
		}

		/// <summary>
		/// Parses a request message.
		/// Object to xml.
		/// </summary>
		/// <param name="in_method_name"></param>
		/// <param name="in_params">Pass this as <see cref="ArrayList"></see> if you need to send an XML_RPC_ARRAY</param>
		public GbxCall(string in_method_name, IEnumerable<object> in_params = null)
		{
			//TODO: Convert to StringBuilder
			this.m_xml = "<?xml version=\"1.0\" ?>\n";
			this.m_xml += "<methodCall>\n";
			this.m_xml += $"<methodName>{in_method_name}</methodName>\n";
			this.m_xml += "<params>\n";

			if (in_params != null)
			{
				foreach (object param in in_params)
				{
					this.m_xml += $"<param>{ParseObject(param)}</param>\n";
				}
			}

			this.m_xml += "</params>";
			this.m_xml += "</methodCall>";
		}

		private string ParseObject(object inParam)
		{
			// open parameter ...
			StringBuilder xmlBuilder = new StringBuilder("<value>");

			if (inParam is string stringParam) // parse type string ...
			{
				xmlBuilder.Append($"<string>{HttpUtility.HtmlEncode(stringParam)}</string>");
			}
			else if (inParam is int intParam) // parse type int32 ...
			{
				xmlBuilder.Append($"<int>{intParam}</int>");
			}
			else if (inParam is double doubleParam) // parse type double ...
			{
				xmlBuilder.Append($"<double>{doubleParam}</double>");
			}
			else if (inParam is bool boolParam)  // parse type bool ...
			{
				xmlBuilder.Append(boolParam ? "<boolean>1</boolean>" : "<boolean>0</boolean>");
			}
			else if (inParam is ArrayList arrayListParam) // parse type array ...
			{
				xmlBuilder.Append("<array><data>");

				foreach (object element in arrayListParam)
				{
					xmlBuilder.Append(ParseObject(element));
				}

				xmlBuilder.Append("</data></array>");
			}
			else if (inParam.GetType() == typeof(Hashtable)) // parse type struct ...
			{
				xmlBuilder.Append("<struct>");

				foreach (object key in ((Hashtable)inParam).Keys)
				{
					xmlBuilder.Append("<member>");
					xmlBuilder.Append($"<name>{key}</name>");
					xmlBuilder.Append(ParseObject(((Hashtable)inParam)[key]));
					xmlBuilder.Append("</member>");
				}

				xmlBuilder.Append("</struct>");
			}
			else if (inParam.GetType() == typeof(byte[])) // parse type of byte[] into base64
			{
				xmlBuilder.Append("<base64>");
				xmlBuilder.Append(Convert.ToBase64String((byte[])inParam));
				xmlBuilder.Append("</base64>");
			}

			// close parameter ...
			// TODO: Might need to convert this to just \n (that's what it was originally)
			xmlBuilder.Append($"</value>{Environment.NewLine}");
			return xmlBuilder.ToString();
		}

		private object ParseXml(XmlElement inParam)
		{
			var val = inParam["value"] ?? inParam;

			if (val["string"] != null) // param of type string ...
			{
				return val["string"].InnerText;
			}

			if (val["int"] != null) // param of type int32 ...
			{
				return Convert.ToInt32(val["int"].InnerText);
			}

			if (val["i4"] != null) // param of type int32 (alternative) ...
			{
				return Convert.ToInt32(val["i4"].InnerText);
			}

			if (val["double"] != null) // param of type double ...
			{
				return Convert.ToDouble(val["double"].InnerText);
			}

			if (val["boolean"] != null) // param of type boolean ...
			{
				return val["boolean"].InnerText == "1";
			}

			if (val["struct"] != null) // param of type struct ...
			{
				Hashtable structure = new Hashtable();

				foreach (XmlElement member in val["struct"])
				{
					// parse each member ...
					string thisMemberName = member["name"]?.InnerText;
					if (!string.IsNullOrWhiteSpace(thisMemberName))
					{
						structure.Add(thisMemberName, ParseXml(member));
					}
				}

				return structure;
			}
			else if (val["array"] != null) // param of type array ...
			{
				ArrayList array = new ArrayList();
				foreach (XmlElement data in val["array"]["data"])
				{
					// parse each data field ...
					array.Add(ParseXml(data));
				}
				return array;
			}
			else if (val["base64"] != null) // param of type base64 ...
			{
				byte[] data = Convert.FromBase64String(val["base64"].InnerText);
				return data;
			}

			return null;
		}

		public string MethodName => this.m_method_name;
		public MessageTypes Type => this.m_type;
		public string Xml => this.m_xml;
		public ArrayList Params => this.m_params;
		public int Size => this.m_xml.Length;

		public int Handle
		{
			get => this.m_handle;
			set
			{
				this.m_handle = value;
			}
		}

		public bool Error => this.m_error;
		public string ErrorString => m_error_string;
		public int ErrorCode => m_error_code;
	}
}
