﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManiaXmlRpc.Models;
using ManiaXmlRpc.Responses;
using Newtonsoft.Json;
using NLog;

namespace ManiaXmlRpc.Clients
{
	public class ManiaPlanetXmlRpcClient : XmlRpcClient, IManiaXmlRpcClient
	{
		private static readonly ILogger _log = LogManager.GetCurrentClassLogger();
		private bool _isConnected = false;
		private bool _isAuthenticated = false;

		public ManiaPlanetXmlRpcClient(string serverAddress, int port) : base(serverAddress, port)
		{
			_log.Debug($"{nameof(ManiaPlanetXmlRpcClient)} constructed for {serverAddress}:{port}");
		}

		public bool IsAuthenticated => _isAuthenticated;

		private async Task ConnectIfNeededAsync()
		{
			if (_isConnected)
			{
				return;
			}

			var errorCode = await this.ConnectAsync();
			this._isConnected = errorCode == 0;
			this.EventOnDisconnectCallback += sender =>
			{
				_log.Warn("Client DISCONNECTED");
				this._isAuthenticated = false;
				this._isConnected = false;
			};
		}

		public async Task<bool> AuthenticateAsync(string username, string password)
		{
			await this.ConnectIfNeededAsync();
			var result = this.Request("Authenticate", new object[] { username, password });
			this._isAuthenticated = result.ErrorCode == 0;
			return this.IsAuthenticated;
		}

		public async Task<IEnumerable<string>> ListMethodsAsync()
		{
			await this.ConnectIfNeededAsync();
			var response = this.Request("system.listMethods");
			switch (response.Params[0])
			{
				case null:
					return Array.Empty<string>();
				case ArrayList:
					{
						var data = ((ArrayList)response.Params[0]).Cast<string>();
						return data;
					}
				default:
					throw new ApplicationException($"Unexpected response type: {response.Params[0].GetType().FullName}");
			}
		}

		public async Task<string> GetMethodHelpAsync(string methodName)
		{
			await this.ConnectIfNeededAsync();
			var response = this.Request("system.methodHelp", new object[] { methodName });
			return response.Params[0]?.ToString();
		}

		public async Task<SystemMethodSignatureResponse> GetMethodSignatureAsync(string methodName)
		{
			await this.ConnectIfNeededAsync();
			var response = this.Request("system.methodSignature", new object[] { methodName });

			// 2021-07-09 JLitster (ReSharper showed me this one...)
			// v--- This is FREAKING cool! ---v
			if (response.Params[0] is not ArrayList { Count: > 0 } paramTypes)
			{
				throw new ApplicationException("Unable to extract method signature info");
			}

			var paramTypeStrings = ((ArrayList)paramTypes[0] ?? new ArrayList()).Cast<string>().ToArray();
			var data = new SystemMethodSignatureResponse
			{
				ReturnType = paramTypeStrings[0],
				ParameterTypes = paramTypeStrings.Skip(1).ToList()
			};

			return data;
		}

		public async Task<GetMaxPlayersResponse> GetMaxPlayersAsync()
		{
			await this.ConnectIfNeededAsync();
			var response = this.Request("GetMaxPlayers");
			var data = JsonConvert.DeserializeObject<GetMaxPlayersResponse>(JsonConvert.SerializeObject(response.Params[0]));
			return data;
		}

		public async Task<GetMapInfoResponse> GetCurrentMapInfoAsync()
		{
			await this.ConnectIfNeededAsync();

			var response = this.Request("GetCurrentMapInfo");
			var data = JsonConvert.DeserializeObject<GetMapInfoResponse>(JsonConvert.SerializeObject(response.Params[0]));
			return data;
		}

		public async Task<GetMapListResponse> GetMapListAsync(int maxItems = 1000, int startIndex = 0)
		{
			await this.ConnectIfNeededAsync();

			var response = this.Request("GetMapList", new object[] { maxItems, startIndex });

			var data = JsonConvert.DeserializeObject<GetMapListResponse>(JsonConvert.SerializeObject(response.Params[0]));
			return data;
		}

		public async Task<bool> ChooseNextMapAsync(string filename)
		{
			if(string.IsNullOrWhiteSpace(filename))
			{
				throw new ArgumentNullException(nameof(filename), $"{filename} is required");
			}

			await this.ConnectIfNeededAsync();

			var response = this.Request("ChooseNextMap", new object[] { filename });
			var data = Convert.ToBoolean(response.Params[0]);
			return data;
		}

		public async Task<int> ChooseNextMapListAsync(IEnumerable<string> filenames)
		{
			object[] nextMaps = filenames?.Cast<object>().ToArray() ?? Array.Empty<object>();
			if(!nextMaps.Any())
			{
				throw new ArgumentException($"{filenames} is required");
			}

			await this.ConnectIfNeededAsync();

			ArrayList xmlRpcParams = new ArrayList(nextMaps);
			var response = this.Request("ChooseNextMapList", new object[]{ xmlRpcParams });
			Console.WriteLine(JsonConvert.SerializeObject(response));
			var data = Convert.ToInt32(response.Params[0]);
			return data;
		}
	}
}