using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NLog;

namespace ManiaXmlRpc.Clients
{
	/// <summary>
	/// XmlRpc client which can send synchronous requests to a XmlRpc server and
	/// receives responses synchronous or asynchronous.
	/// Supports "parallel" receiving of callbacks from the server.
	/// - Written 2006 by Florian Schnell -
	/// (Adapted to .NET 5 by Jeff Litster)
	/// </summary>
	public class XmlRpcClient
	{
		private static readonly ILogger _log = LogManager.GetCurrentClassLogger();

		private Socket tcpSocket;
		private int requests;
		private byte[] m_buffer;
		private IAsyncResult asyncResult;
		private readonly AutoResetEvent callRead = new(false);
		private readonly Hashtable responses = new();
		public event GbxCallbackHandler EventGbxCallback;
		public event OnDisconnectHandler EventOnDisconnectCallback;
		private readonly Hashtable callbackList = new();
		private readonly CancellationToken _cancelToken = CancellationToken.None;

		private void OnDisconnectCallback()
		{
			EventOnDisconnectCallback?.Invoke(this);
		}

		private void OnGbxCallback(GbxCallbackEventArgs e)
		{
			EventGbxCallback?.Invoke(this, e);
		}

		/// <summary>
		/// Frees all resources and disconnects from the rpc server.
		/// </summary>
		public void Dispose()
		{
			tcpSocket.Close();
			EventGbxCallback = null;
			EventOnDisconnectCallback = null;
		}

		/// <summary>
		/// Initializes the client with port and ip address.
		/// Use Connect method to set up a connection.
		/// </summary>
		/// <param name="inAddress">IP address to initialize with.</param>
		/// <param name="inPort">Port to initialize the client with.</param>
		public XmlRpcClient(string inAddress, int inPort)
		{
			this.IP = inAddress;
			this.Port = inPort;
		}

		/// <summary>
		/// Creates a connection to the server.
		/// </summary>
		/// <returns>0:success, 1:wrong socket, 2:wrong protocol version</returns>
		public async Task<int> ConnectAsync()
		{
			// try to connect on the socket ...
			bool connectedSuccessfully = await this.SocketConnectAsync(this.IP, this.Port);
			_cancelToken.ThrowIfCancellationRequested();

			if (!connectedSuccessfully)
			{
				return 1;
			}

			bool handshakeWasSuccessful = await this.HandshakeAsync();
			if (!handshakeWasSuccessful)
			{
				return 2;
			}

			m_buffer = new byte[8];
			this.asyncResult = tcpSocket.BeginReceive(m_buffer, 0, m_buffer.Length, SocketFlags.None, OnDataArrive, null);
			return 0;
		}

		/// <summary>
		/// Establishes a socket connection.
		/// </summary>
		private async Task<bool> SocketConnectAsync(string inAddress, int inPort)
		{
			// create end point ...
			var remoteEndPoint = new IPEndPoint(IPAddress.Parse(inAddress), inPort);

			try
			{
				// create a socket for the remote end point ...
				tcpSocket = new Socket(remoteEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

				// connect onto the remote end point ...
				await tcpSocket.ConnectAsync(remoteEndPoint, _cancelToken);

				// connection established successfully!
				return true;
			}
			catch
			{
				// could not connect!
				return false;
			}
		}

		/// <summary>
		/// Reads the header of the protocol and checks if for compability.
		/// </summary>
		/// <returns>Returns whether a connection is possible.</returns>
		private async Task<bool> HandshakeAsync()
		{
			// are we connected already?
			if (tcpSocket.Connected)
			{
				// get size ...
				byte[] Buffer = new byte[4];
				await tcpSocket.ReceiveAsync(Buffer, SocketFlags.None, this._cancelToken);
				int Size = System.BitConverter.ToInt32(Buffer, 0);

				// get handshake ...
				byte[] HandshakeBuffer = new byte[Size];
				await tcpSocket.ReceiveAsync(HandshakeBuffer, SocketFlags.None, this._cancelToken);
				string Handshake = Encoding.UTF8.GetString(HandshakeBuffer);

				// check if compatible ...
				return Handshake == "GBXRemote 2";
			}

			throw new NotConnectedException();
		}

		/// <summary>
		/// Closes the socket connection.
		/// </summary>
		public void Disconnect()
		{
			this.tcpSocket.Close();
		}

		private void OnDataArrive(IAsyncResult iar)
		{
			_log.Debug($"{nameof(OnDataArrive)} fired");
			//_log.Info($"result info:{Environment.NewLine}{JsonConvert.SerializeObject(iar)}");

			// end receiving and check if connection's still alive ...
			try
			{
				tcpSocket.EndReceive(iar);

				// receive the message from the server ...
				GbxCall call = XmlRpc.ReceiveCall(this.tcpSocket, m_buffer);

				// watch out for the next calls ...
				m_buffer = new byte[8];
				asyncResult = tcpSocket.BeginReceive(m_buffer, 0, m_buffer.Length, SocketFlags.None, new AsyncCallback(OnDataArrive), null);

				if (call.Type == MessageTypes.Callback)
				{
					_log.Info($"{MessageTypes.Callback} call type happened.  Trying to fire the GbxCallback");
					// throw new event ...
					var eArgs = new GbxCallbackEventArgs(call);
					OnGbxCallback(eArgs);
				}
				else
				{
					_log.Info($"Non-callback: (type = '{call.Type}')");
					// add the response to the queue ...
					lock (this)
					{
						responses.Add(call.Handle, call);
					}

					// callback if any method was set ...
					if (callbackList[call.Handle] != null)
					{
						_log.Warn($"Callback was defined for this handle ({call.Handle}) - attempting to call it.");
						var callback = (GbxCallCallbackHandler)callbackList[call.Handle];
						_log.Info($"====> Callback method name: {callback.Method.Name}");
						// Fire and forget
						Task.Run(() => callback.Invoke(call), _cancelToken).ConfigureAwait(false);
						callbackList.Remove(call.Handle);
					}
				}
			}
			catch (Exception ex)
			{
				_log.Error(ex, $"Unexpected error while handling the OnDataArrive event. Closing connection and firing the OnDisconnectCallback{Environment.NewLine}{ex.Message}{Environment.NewLine}{ex.StackTrace}");
				// something went wrong :S
				tcpSocket.Close();

				// release a disconnect event ...
				OnDisconnectCallback();
			}
			finally
			{
				// we received something :)
				callRead.Set();
			}
		}

		/// <summary>
		/// (Dis)activates callbacks from the server.
		/// </summary>
		/// <param name="inState">Whether to receive callbacks from the server.</param>
		/// <returns>Returns new callback state</returns>
		public bool EnableCallbacks(bool inState)
		{
			var EnableCall = new GbxCall("EnableCallbacks", new object[] { inState }) { Handle = --this.requests };
			return (XmlRpc.SendCall(this.tcpSocket, EnableCall) != 0);
		}

		/// <summary>
		/// Sends a request to the server and blocks until a response has been received.
		/// </summary>
		/// <param name="inMethodName">The method to call.</param>
		/// <param name="inParams">Parameters describing your request.</param>
		/// <returns>Returns a response object from the server.</returns>
		public GbxCall Request(string inMethodName, object[] inParams = null)
		{
			// reset event ...
			callRead.Reset();

			// send the call and remember the handle we are waiting on ...
			GbxCall Request = new GbxCall(inMethodName, inParams) { Handle = --this.requests };
			int handle = XmlRpc.SendCall(this.tcpSocket, Request);

			// wait until we received the call ...
			do
			{
				callRead.WaitOne();
			} while (responses[handle] == null && tcpSocket.Connected);

			// did we get disconnected ?
			if (!tcpSocket.Connected)
				throw new NotConnectedException();

			// get the call and return it ...
			return GetResponse(handle);
		}

		/// <summary>
		/// Sends a Request and does not wait for a response of the server.
		/// The response will be written into a buffer or you can set a callback method
		/// that will be executed.
		/// </summary>
		/// <param name="inMethodName">The method to call.</param>
		/// <param name="inParams">Parameters describing your request.</param>
		/// <param name="callbackHandler">An optional delegate which is called when the response is available otherwise set it to null.</param>
		/// <returns>Returns a handle to your request.</returns>
		public int AsyncRequest(string inMethodName, object[] inParams, GbxCallCallbackHandler callbackHandler)
		{
			_log.Debug("AsyncRequest being made");

			// send the call and remember the handle ...
			GbxCall Request = new GbxCall(inMethodName, inParams) { Handle = --this.requests };
			int handle = XmlRpc.SendCall(this.tcpSocket, Request);

			lock (this)
			{
				if (handle != 0)
				{
					// register a callback on this request ...
					if (callbackHandler != null)
					{
						_log.Debug($"callback exists, adding to callback list for handle ({handle})");
						callbackList.Add(handle, callbackHandler);
					}

					// return handle id ...
					return handle;
				}
				else
				{
					return 0;
				}
			}
		}

		/// <summary>
		/// Gets an asynchron response from the list.
		/// </summary>
		/// <param name="inHandle">The handle which was returned from AsyncRequest.</param>
		/// <returns>Returns the cached response.</returns>
		public GbxCall GetResponse(int inHandle)
		{
			return (GbxCall)responses[inHandle];
		}

		public string IP { get; set; }
		public int Port { get; set; }
	}

	public delegate void GbxCallbackHandler(object o, GbxCallbackEventArgs e);
	public delegate void OnDisconnectHandler(object o);
	public delegate void GbxCallCallbackHandler(GbxCall res);

	public class NotConnectedException : Exception
	{}

	public class GbxCallbackEventArgs : EventArgs
	{
		public readonly GbxCall Response;

		public GbxCallbackEventArgs(GbxCall response)
		{
			Response = response;
		}
	}
}
