﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ManiaXmlRpc.Models;
using ManiaXmlRpc.Responses;

namespace ManiaXmlRpc.Clients
{
	//=============================================
	/*
	 *			TODO: LEFT OFF HERE!!!
	 *		Implement "EnableCallbacks"
	 *		and add the events that will support it.
	 */
	//=============================================
	public interface IManiaXmlRpcClient
	{
		event GbxCallbackHandler EventGbxCallback;
		event OnDisconnectHandler EventOnDisconnectCallback;

		/// <summary>
		/// This MUST be called before calling any of the other methods.
		/// </summary>
		/// <param name="username">The ManiaServer username</param>
		/// <param name="password">The ManiaServer password</param>
		/// <returns></returns>
		Task<bool> AuthenticateAsync(string username, string password);

		Task<IEnumerable<string>> ListMethodsAsync();
		Task<string> GetMethodHelpAsync(string methodName);
		Task<SystemMethodSignatureResponse> GetMethodSignatureAsync(string methodName);

		Task<GetMaxPlayersResponse> GetMaxPlayersAsync();

		Task<GetMapInfoResponse> GetCurrentMapInfoAsync();
		Task<GetMapListResponse> GetMapListAsync(int maxItems = 1000, int startIndex = 0);
		
		/// <summary>
		/// Sets the next map
		/// </summary>
		/// <param name="filename">The 'filename' provided by <see cref="GetMapListAsync"></see></param>
		/// <returns>True if the server accepted the choice; False otherwise</returns>
		Task<bool> ChooseNextMapAsync(string filename);

		/// <summary>
		/// Sets the next maps (if the requested filenames are available)
		/// (Get the filenames from <see cref="GetMapListAsync"></see>)
		/// </summary>
		/// <param name="filenames">The collection of filenames to set.</param>
		/// <returns>The number of filenames successfully added as next maps</returns>
		Task<int> ChooseNextMapListAsync(IEnumerable<string> filenames);
	}
}